import TodoForm from "../../components/TodoForm";
import "./styles.scss";
import TodoPageHeader from "../../components/TodoPageHeader";
import { useHistory, useParams } from "react-router-dom";
import { todosStore } from "../../store/todos";

const EditTodoPage = () => {
  const { id } = useParams<{ id: string }>();
  const history = useHistory();

  const todo = id !== typeof "string" ? todosStore.getTodoById(id) : undefined;

  if (!todo) {
    history.push("/");
  }

  return (
    <div className="edit-todo-page">
      <TodoPageHeader title="Редактировать задачу" />
      <TodoForm todo={todo} submitButtonTitle="Сохранить" />
    </div>
  );
};

export default EditTodoPage;
