import TodoForm from "../../components/TodoForm";
import "./styles.scss";
import TodoPageHeader from "../../components/TodoPageHeader";

const AddTodoPage = () => {
  return (
    <div className="add-todo-page">
      <TodoPageHeader title="Добавить задачу" />
      <TodoForm />
    </div>
  );
};

export default AddTodoPage;
