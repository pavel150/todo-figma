import { Link } from "react-router-dom";
import TodosFilter from "../../components/TodosFilter";
import TodosList from "../../components/TodosList";
import "./styles.scss";

const TodosListPage = () => {
  return (
    <div className="todo-list-page">
      <Link className="todo-list-page__add-button" to="add">
        Добавить задачу
      </Link>
      <TodosFilter />
      <TodosList />
    </div>
  );
};

export default TodosListPage;
