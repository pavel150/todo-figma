import AddTodoPage from "../pages/AddTodoPage";
import EditTodoPage from "../pages/EditTodoPage";
import TodosListPage from "../pages/TodosListPage";

const ROUTES = [
  { path: "", exact: true, component: TodosListPage },
  { path: "add", component: AddTodoPage },
  { path: "edit/:id", component: EditTodoPage },
];

export default ROUTES;
