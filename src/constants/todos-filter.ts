export const TODOS_FILTERS = [
  { key: "all", displayName: "Все" },
  { key: "uncompleted", displayName: "Текущие задачи" },
  { key: "completed", displayName: "Завершенные" },
] as const;
