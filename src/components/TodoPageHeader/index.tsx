import { FC } from "react";
import { Link } from "react-router-dom";
import "./styles.scss";

interface TodoPageHeaderProps {
  title: string;
}

const TodoPageHeader: FC<TodoPageHeaderProps> = ({ title }) => {
  return (
    <header className="todo-page-header">
      <h2 className="todo-page-header__title">{title}</h2>
      <Link to="/" className="todo-page-header__cancel">
        Отменить
      </Link>
    </header>
  );
};

export default TodoPageHeader;
