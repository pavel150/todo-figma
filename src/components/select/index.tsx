import clsx from "clsx";
import { FC, useState } from "react";
import styles from "./select.module.scss";

type SelectProps = {
  options: string[];
  initialOpion?: number;
  onOptiionChange?: (index: number) => void;
  title?: string;
  className?: string;
} & Pick<JSX.IntrinsicElements["button"], "onBlur">;

const Select: FC<SelectProps> = ({
  options,
  initialOpion,
  onOptiionChange,
  title,
  className,
  onBlur,
}) => {
  const [open, setOpen] = useState(false);
  const [current, setCurrent] = useState(initialOpion ?? 0);

  return (
    <div className={clsx(styles.wrapper, className)}>
      <button
        onClick={() => {
          setOpen((prev) => !prev);
        }}
        className={clsx(styles.head, open && styles.open)}
        type="button"
        onBlur={onBlur}
      >
        {options[current]}
        {title && <span className={styles.title}>{title}</span>}
      </button>
      <div className={clsx(styles.options, open && styles.optionsOpen)}>
        {options.map((option, i) => (
          <button
            key={i}
            className={clsx(
              styles.option,
              current === i && styles.activeOption
            )}
            type="button"
            tabIndex={open ? 0 : -1}
            onClick={() => {
              setOpen(false);
              setCurrent(i);
              onOptiionChange && onOptiionChange(i);
            }}
          >
            {option}
          </button>
        ))}
      </div>
    </div>
  );
};

export default Select;
