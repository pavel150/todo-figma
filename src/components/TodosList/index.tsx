import { observer } from "mobx-react-lite";
import { todosStore } from "../../store/todos";
import { TodoCard } from "./TodoCard";
import "./styles.scss";

const TodosList = observer(() => {
  return (
    <div className="todos-list">
      {todosStore.filtered.map((todo) => (
        <TodoCard todo={todo} key={todo.id} />
      ))}
    </div>
  );
});

export default TodosList;
