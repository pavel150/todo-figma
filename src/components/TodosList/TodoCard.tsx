import { observer } from "mobx-react-lite";
import { FC } from "react";
import { Link } from "react-router-dom";
import { ITodo } from "../../store/types";
import Button from "../button";

export const TodoCard: FC<{ todo: ITodo }> = observer(({ todo }) => {
  return (
    <div className="todo-card">
      <header className="todo-card__header">
        <h3 className="todo-card__title">{todo.title}</h3>
        <div className="todo-card__info">
          <div className="todo-card__info-badge todo-card__info-badge--dark">{`Приоритет ${todo.priority}`}</div>
          {todo.completeBefore && (
            <div className="todo-card__info-badge">
              {new Date(todo.completeBefore).toLocaleDateString()}
            </div>
          )}
          <div className="todo-card__info-badge">
            {todo.completed ? "Выполнено" : "Не выплнено"}
          </div>
        </div>
      </header>
      <p className="todo-card__body">{todo.body}</p>
      <div className="todo-card__actions">
        <Link
          to={`edit/${todo.id}`}
          className="todo-card__action todo-card__action--link"
        >
          Редактировать
        </Link>
        <Button className="todo-card__action" onClick={() => todo.toggle()}>
          {todo.completed ? "Не выполнено" : "Выполнить"}
        </Button>
      </div>
    </div>
  );
});
