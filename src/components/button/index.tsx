import clsx from "clsx";
import React, { FC } from "react";
import styles from "./button.module.scss";

interface ButtonProps {
  variant?: "filled" | "outline";
  colorScheme?: "dark" | "light";
  size?: "small" | "normal";
}

const Button: FC<React.ComponentPropsWithoutRef<"button"> & ButtonProps> = ({
  variant = "filled",
  colorScheme = "dark",
  size = "normal",
  children,
  ...rest
}) => {
  return (
    <button
      {...rest}
      className={clsx(
        styles.button,
        variant === "outline" && styles.outline,
        colorScheme === "light" && styles.light,
        size === "small" && styles.small,
        rest.className
      )}
    >
      {children}
    </button>
  );
};

export default Button;
