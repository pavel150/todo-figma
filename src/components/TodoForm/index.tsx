import clsx from "clsx";
import { Field, Form, Formik, FieldProps } from "formik";
import { ChangeEvent, FC } from "react";
import { useHistory } from "react-router-dom";
import { todosStore } from "../../store/todos";
import { ITodo } from "../../store/types";
import Button from "../button";
import Select from "../select";
import TextField from "../TextField";
import "./styles.scss";
import * as Yup from "yup";
import { convertDateToDDMMYYYY } from "../../utils/date";
interface TodoFormProps {
  todo?: ITodo;
  className?: string;
  submitButtonTitle?: string;
}

const TodoForm: FC<TodoFormProps> = ({
  todo,
  className,
  submitButtonTitle = "Создать",
}) => {
  const history = useHistory();
  console.log(todo?.priority);

  const initialValues = {
    title: todo ? todo.title : "",
    body: todo ? todo.body : "",
    completeBefore: todo?.completeBefore
      ? new Date(todo.completeBefore).toLocaleDateString()
      : "",
    priority: todo ? todo.priority : 1,
  };

  const validationSchema = Yup.object({
    title: Yup.string().required("Введите название задачи"),
    body: Yup.string().required("Введите описание задачи"),
    completeBefore: Yup.mixed().test(
      "date",
      "Введиту дату в формате дд.мм.гггг",
      (value) => {
        const dateRegex = /\d{1,2}\.\d{1,2}\.\d{4}/;

        if (value === undefined) {
          return true;
        }

        // test if can parse date
        try {
          const date = Date.parse(convertDateToDDMMYYYY(value));
          if (isNaN(date)) {
            throw new Error();
          }
        } catch (error) {
          return false;
        }

        // test regex
        return typeof value === "string" && dateRegex.test(value);
      }
    ),
    priority: Yup.number().required(),
  });

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={({ title, body, completeBefore, priority }) => {
        const resultValues = {
          title,
          body,
          completeBefore: completeBefore
            ? Date.parse(convertDateToDDMMYYYY(completeBefore))
            : null,
          priority,
        };

        if (todo) {
          todo.edit(resultValues);
        } else {
          todosStore.add(resultValues);
        }
        history.push("/");
      }}
    >
      <Form className={clsx("todo-form", className)}>
        <Field
          name="title"
          component={({ field, form }: FieldProps) => {
            const { error, touched } = form.getFieldMeta("title");
            const showError = error && touched ? error : undefined;

            return (
              <TextField
                {...field}
                placeholder="Название задачи"
                error={showError}
              />
            );
          }}
        />
        <Field
          name="body"
          component={({ field, form }: FieldProps) => {
            const { error, touched } = form.getFieldMeta("body");
            const showError = error && touched ? error : undefined;

            return (
              <TextField
                {...field}
                variant="textarea"
                placeholder="Описание задачи"
                error={showError}
                className="todo-form__body-field"
              />
            );
          }}
        />

        <div className="todo-form__row">
          <Field
            name="completeBefore"
            component={({ field, form }: FieldProps) => {
              const { error, touched } = form.getFieldMeta("completeBefore");
              const showError = error && touched ? error : undefined;

              return (
                <TextField
                  {...field}
                  onChange={(e: ChangeEvent<HTMLInputElement>) => {
                    form.setFieldValue("completeBefore", e.target.value);
                  }}
                  placeholder="Дедлайн"
                  error={showError}
                  className="todo-form__date-field"
                />
              );
            }}
          />
          <Field
            name="priority"
            component={({ form }: FieldProps) => (
              <Select
                className="todo-form__select"
                options={[
                  "Приоритет 1",
                  "Приоритет 2",
                  "Приоритет 3",
                  "Приоритет 4",
                  "Приоритет 5",
                ]}
                title="Приоритет"
                onOptiionChange={(i) => {
                  form.setFieldValue("priority", i + 1);
                }}
                initialOpion={todo?.priority ? todo.priority - 1 : 0}
              />
            )}
          />

          <Button type="submit" className="todo-form__submit">
            {submitButtonTitle}
          </Button>
        </div>
      </Form>
    </Formik>
  );
};

export default TodoForm;
