import clsx from "clsx";
import { observer } from "mobx-react-lite";
import { TODOS_FILTERS } from "../../constants/todos-filter";
import { todosStore } from "../../store/todos";
import "./styles.scss";

const TodosFilter = observer(() => {
  return (
    <div className="todos-filter">
      {TODOS_FILTERS.map((filter, idx) => (
        <button
          className={clsx(
            "todos-filter__item",
            todosStore.filter === filter.key && "todos-filter__item--active"
          )}
          key={idx}
          onClick={() => todosStore.setFilter(filter.key)}
        >
          <span>{filter.displayName}</span>
        </button>
      ))}
    </div>
  );
});

export default TodosFilter;
