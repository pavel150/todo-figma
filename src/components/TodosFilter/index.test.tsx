import { fireEvent, render, screen } from "@testing-library/react";
import TodosFilter from ".";
import { TODOS_FILTERS } from "../../constants/todos-filter";
import { todosStore } from "../../store/todos";

jest.mock("../../store/todos", () => {
  const mockStore = {
    filter: "all",
    setFilter: function (filter: string) {
      this.filter = filter;
    },
  };
  return {
    todosStore: mockStore,
  };
});

describe("TodosFilter", () => {
  it("should render TodosFilter", async () => {
    render(<TodosFilter />);

    TODOS_FILTERS.forEach(({ displayName }) => {
      expect(screen.getByText(displayName)).toHaveTextContent(displayName);
    });
  });

  it("should set filter to last(uncompleted)", () => {
    render(<TodosFilter />);

    // before click store.filter === first
    expect(todosStore.filter).toBe(TODOS_FILTERS[0].key);

    console.log(todosStore.todos);
    const lastFilter = screen.getByText(TODOS_FILTERS[2].displayName);

    // click on last filter
    fireEvent(lastFilter, new MouseEvent("click", { bubbles: true }));

    // after store.filter === last
    expect(todosStore.filter).toBe(TODOS_FILTERS[2].key);
  });
});
