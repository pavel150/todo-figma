import { ComponentPropsWithoutRef, ComponentType } from "react";

type FieldVariants = Pick<JSX.IntrinsicElements, "input" | "textarea">;

type FieldVariantType<P = any> =
  | {
      [K in keyof FieldVariants]: P extends FieldVariants[K] ? K : never;
    }[keyof FieldVariants]
  | ComponentType<P>;

type TextFieldProps<T extends FieldVariantType> = {
  variant?: T;
  placeholder: string;
  type?: "text" | "date" | "password" | "email";
  error?: string;
};

export interface ITextField {
  <T extends FieldVariantType = "input">({
    variant,
    ...rest
  }: TextFieldProps<T> &
    Partial<
      Omit<ComponentPropsWithoutRef<T>, keyof TextFieldProps<T>>
    >): JSX.Element;
}
