import clsx from "clsx";
import { ITextField } from "./types";
import "./styles.scss";

const TextField: ITextField = ({
  variant,
  placeholder,
  className,
  type,
  error,
  ...rest
}) => {
  const Component = variant || "input";

  return (
    <div
      className={clsx("text-field", error && "text-field--error", className)}
    >
      <Component
        {...rest}
        type={type}
        className={"text-field__input"}
        placeholder=" "
      />
      <div className={"text-field__placeholder"}>{placeholder}</div>
      {error && <div className={"text-field__error"}>{error}</div>}
    </div>
  );
};

export default TextField;
