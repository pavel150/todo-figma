import styles from "./header.module.scss";

const Header = () => {
  return (
    <header className={styles.header}>
      <div className={styles.inner}>
        <div className={styles.title}>TODOAPP</div>
      </div>
    </header>
  );
};

export default Header;
