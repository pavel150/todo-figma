import TodosList from "../../../store/todos/todosList";
import {
  ITodoEditProps,
  ITodosList,
  TNewTodoProps,
} from "../../../store/types";

const testTodo: TNewTodoProps = {
  title: "new todo title",
  body: "new todo body",
  priority: 2,
};

let store: ITodosList;

it("should create store", () => {
  store = new TodosList();

  expect(store.todos.length).toBe(0);
});

it("should add todo to store", () => {
  store.add(testTodo);

  expect(store.todos.length).toBe(1);
  expect(store.todos[0].title).toBe(testTodo.title);
  expect(store.todos[0].body).toBe(testTodo.body);
  expect(store.todos[0].completed).toBeFalsy();
  expect(store.todos[0].priority).toBe(testTodo.priority);
});

it("should remove todo from store", () => {
  const todoIdToRemove = store.todos[0].id;
  store.remove(todoIdToRemove);

  expect(store.todos.length).toBe(0);
});

it("should return list of completed todos", () => {
  // add 2 completed todos
  store.add({
    ...testTodo,
    completed: true,
  });
  store.add({
    ...testTodo,
    completed: true,
  });

  // add 1 uncompleted todo
  store.add(testTodo);

  expect(store.todos.length).toBe(3);
  expect(store.completed.length).toBe(2);
  expect(store.uncompleted.length).toBe(1);
});

it("should edit todo", () => {
  // before edit
  expect(store.todos[2].title).toBe(testTodo.title);
  expect(store.todos[2].body).toBe(testTodo.body);
  expect(store.todos[2].priority).toBe(testTodo.priority);

  const changes: ITodoEditProps = {
    title: "edit title",
    body: "edit body",
    priority: 1,
  };

  store.todos[2].edit(changes);

  // after edit
  expect(store.todos[2].title).toBe(changes.title);
  expect(store.todos[2].body).toBe(changes.body);
  expect(store.todos[2].priority).toBe(changes.priority);
});
