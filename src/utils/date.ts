/**
 * @param date date string in dd.mm.yyyy format
 * @returns date string in mm.dd.yyyy format
 */
export const convertDateToDDMMYYYY = (date: string) => {
  const dateArr = date.split(".");
  return [dateArr[1], dateArr[0], dateArr[2]].join(".");
};
