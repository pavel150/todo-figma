import { TODOS_FILTERS } from "../constants/todos-filter";

export interface ITodoBase {
  id: string;
  title: string;
  body: string;
  completed: boolean;
  priority: 1 | 2 | 3 | 4 | null;
  completeBefore: number | null;
}

export type ITodoEditProps = Omit<Partial<ITodoBase>, "id" | "completed">;
export interface ITodo extends ITodoBase {
  store: ITodosList;
  toggle(): void;
  edit(changes: ITodoEditProps): void;
}

export type TNewTodoProps = Partial<ITodoBase>;

export interface ITodosList {
  todos: ITodo[];
  filter: typeof TODOS_FILTERS[number]["key"];

  add(todo: TNewTodoProps): void;
  remove(id: ITodoBase["id"]): void;
  setFilter(filter: ITodosList["filter"]): void;

  completed: ITodo[];
  uncompleted: ITodo[];
  filtered: ITodo[];

  getTodoById(id: ITodo["id"]): ITodo | undefined;
}
