import { createContext } from "react";
import TodosList from "./todosList";

export const todosStore = new TodosList();

export const TodosStoreContext = createContext(todosStore);
