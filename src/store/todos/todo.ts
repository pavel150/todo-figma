import { makeAutoObservable } from "mobx";
import { nanoid } from "nanoid";
import { ITodo, ITodoEditProps, ITodosList, TNewTodoProps } from "../types";

class Todo implements ITodo {
  id;
  title;
  body;
  completed;
  completeBefore;
  priority;
  store;

  constructor(
    store: ITodosList,
    { id, title, body, completed, completeBefore, priority }: TNewTodoProps
  ) {
    makeAutoObservable(this);
    this.id = id ?? nanoid();
    this.title = title ?? "";
    this.body = body ?? "";
    this.completed = completed ?? false;
    this.completeBefore = completeBefore ?? null;
    this.priority = priority ?? 1;
    this.store = store;
  }

  toggle() {
    this.completed = !this.completed;
  }

  edit(changes: ITodoEditProps) {
    Object.assign(this, changes);
  }
}

export default Todo;
