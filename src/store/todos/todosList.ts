import { makeAutoObservable } from "mobx";
import Todo from "./todo";
import { ITodo, ITodosList, TNewTodoProps } from "../types";

class TodosList implements ITodosList {
  todos: ITodo[] = [];
  filter: ITodosList["filter"] = "all";

  constructor() {
    makeAutoObservable(this);
  }

  add(todo: TNewTodoProps) {
    this.todos.push(new Todo(this, todo));
  }

  remove(id: ITodo["id"]) {
    const removeIndex = this.todos.findIndex((todo) => todo.id === id);
    this.todos.splice(removeIndex, 1);
  }

  setFilter(filter: ITodosList["filter"]) {
    this.filter = filter;
  }

  get completed() {
    return this.todos.filter((todo) => todo.completed);
  }

  get uncompleted() {
    return this.todos.filter((todo) => !todo.completed);
  }

  get filtered() {
    switch (this.filter) {
      case "completed":
        return this.completed;

      case "uncompleted":
        return this.uncompleted;

      case "all":
      default:
        return this.todos;
    }
  }

  getTodoById(id: ITodo["id"]) {
    return this.todos.find((todo) => todo.id === id);
  }
}

export default TodosList;
