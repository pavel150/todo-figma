import { FC } from "react";
import Header from "../../components/header";
import styles from "./main.module.scss";

const MainLayout: FC = ({ children }) => {
  return (
    <div className={styles.wrapper}>
      <Header />
      <div className={styles.contentWrapper}>
        <main className={styles.content}>{children}</main>
      </div>
    </div>
  );
};

export default MainLayout;
