import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import MainLayout from "./layouts/main";
import ROUTES from "./constants/routes";

const App = () => {
  return (
    <Router>
      <MainLayout>
        <Switch>
          {ROUTES.map((route, idx) => (
            <Route
              path={"/" + route.path}
              exact={route.exact}
              component={route.component}
              key={idx}
            />
          ))}
        </Switch>
      </MainLayout>
    </Router>
  );
};

export default App;
